# auto-efistub

This script only work with NVMe SSD's because I'm too lazy to do regular expressions with HDD/SSD (sdX).  
This script basically add a EFI entry using efibootmgr so you don't need grub to boot (achieving quicker and silent boot).  
```console
./install.sh
```
