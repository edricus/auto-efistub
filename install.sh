#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

ROOTPART=$(cat /etc/mtab | grep ' / ' | cut -d' ' -f1)
EFIPART=$(grep /boot/efi /etc/mtab | cut -d' ' -f1)
DISTRO="$(lsb_release -a 2>/dev/null | grep 'Distributor ID' | cut -f2)"
EFIPATH=/boot/efi/EFI/"$DISTRO"
DISKNAME=${EFIPART::-2}

if [ -z $(echo $DISKNAME | grep nvme) ]; then
	echo "This script only work with NVMe SSDs"
	exit
fi

# Delete old efi entry
if [[ ! -z $(efibootmgr | grep "$DISTRO") ]]; then
	BOOTNUM=$(sudo efibootmgr | grep Debian | cut -d' ' -f1 | cut -c 8- | sed s/*//)
	efibootmgr --bootnum $BOOTNUM --delete-bootnum 1> /dev/null
	printf 'Deleted old EFI entry...\n\n'
fi

# Copy vmlinuz and initrd in EFI directory everytime kernel is rebuilding
mkdir -p $EFIPATH
cp /vmlinuz /initrd.img $EFIPATH
mkdir -p /etc/kernel/postinst.d
mkdir -p /etc/kernel/postrm.d
mkdir -p /etc/kernel/postupdate.d
mkdir -p /etc/initramfs/post-update.d
cat << EOF > zz-update-efistub
#!/bin/bash
echo "Copying initrd.img and vmlinuz to /boot/efi/EFI/$DISTRO/ ..."
cp /vmlinuz /initrd.img /boot/efi/EFI/$DISTRO/
EOF
cp zz-update-efistub /etc/kernel/postinst.d/
cp zz-update-efistub /etc/kernel/postrm.d/
cp zz-update-efistub /etc/kernel/postupdate.d/
cp zz-update-efistub /etc/initramfs/post-update.d/
rm -rf zz-update-efistub
update-initramfs -u

EFIPARTNUMBER=$(echo "${EFIPART: -1}")
ROOTPARTUUID=$(sudo blkid | grep $ROOTPART | cut -d' ' -f9)
VMLINUZPATH=$(echo ${EFIPATH//\//\\}\\vmlinuz.img | cut -c 10-)
INITRDPATH=$(echo ${EFIPATH//\//\\}\\initrd.img | cut -c 10-)
efibootmgr -c -g -L "$DISTRO" \
	-d "$DISKNAME" \
	-p "$EFIPARTNUMBER" \
	-l "$VMLINUZPATH" \
	-u "root=$ROOTPARTUUID rw quiet splash rootfstype=ext4 add_efi_memmap initrd=$INITRDPATH"
printf "\nAdded new entry '$DISTRO'\n"

# Debug
#echo root partition : $ROOTPART
#echo efi partition : $EFIPART
#echo distribution : $DISTRO
#echo efi path : $EFIPATH
#echo disk name : $DISKNAME
#echo efi partition number : $EFIPARTNUMBER
#echo partuuid of root : $ROOTPARTUUID
